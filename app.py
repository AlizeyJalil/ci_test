from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/', methods=['GET'])
def homepage():
    if request.method == 'GET':
        return render_template('homepage.html')

# app.run(debug=True, )

